
FROM node:13.3.0 AS compile-image

WORKDIR /opt/ng
COPY package.json package-lock.json ./
RUN npm install

ENV PATH="./node_modules/.bin:$PATH" 

COPY . ./
RUN ng build --prod

FROM nginx:latest
COPY default.conf /etc/nginx/conf.d/default.conf
COPY --from=compile-image /opt/ng/dist/ms-front /usr/share/nginx/html